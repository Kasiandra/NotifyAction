#! /usr/bin/python
import dbus
import time
import os
from gi.repository import GLib
from dbus.mainloop.glib import DBusGMainLoop

def notifications(bus, message):
    arg = message.get_args_list()

    x = 0
    for value in arg:
        if value == 'Direct Message':
            os.system("echo command")
            time.sleep(10)
        if value == 'Thunderbird':
            os.system("echo command")
            time.sleep(10)
        if value == 'Eingehender Anruf':
            x = 0
            while x < 20:
                x += 1
                os.system("echo command")
                time.sleep(0.5)
            time.sleep(10)

DBusGMainLoop(set_as_default=True)

bus = dbus.SessionBus()
bus.add_match_string_non_blocking("eavesdrop=true, interface='org.freedesktop.Notifications', member='Notify'")
bus.add_message_filter(notifications)

mainloop = GLib.MainLoop()
mainloop.run()